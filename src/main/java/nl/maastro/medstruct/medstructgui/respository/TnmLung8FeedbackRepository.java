package nl.maastro.medstruct.medstructgui.respository;

import nl.maastro.medstruct.medstructgui.entity.TnmLung8FeedbackEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface TnmLung8FeedbackRepository extends JpaRepository<TnmLung8FeedbackEntity, Long> {
}
