package nl.maastro.medstruct.medstructgui.constants;

public class MedStructConstants {

    public static final String LAYER_CONTEXT = "context";

    public static final String LAYER_CONTEXT_TARGET = "target";

    public static final String LAYER_CONTEXT_MODIFIERS = "modifiers";

    public static final String LAYER_CONTEXT_CATEGORY = "category";

    public static final String LAYER_CONTEXT_TOKENS = "tokens";

    public static final String LAYER_MEASUREMENTS = "measurements";


}
