package nl.maastro.medstruct.medstructgui.constants;

public enum  Laterality {

    LEFT, RIGHT, NONE, UNKNOWN
}
