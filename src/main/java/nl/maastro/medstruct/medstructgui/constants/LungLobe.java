package nl.maastro.medstruct.medstructgui.constants;

public enum LungLobe {

    SLL, ILL, SLR, ML, ILR
}

