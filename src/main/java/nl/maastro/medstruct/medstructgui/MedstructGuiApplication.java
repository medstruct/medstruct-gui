package nl.maastro.medstruct.medstructgui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class MedstructGuiApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedstructGuiApplication.class, args);
	}

}
