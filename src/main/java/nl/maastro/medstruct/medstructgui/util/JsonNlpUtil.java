package nl.maastro.medstruct.medstructgui.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static nl.maastro.medstruct.medstructgui.constants.MedStructConstants.LAYER_CONTEXT_CATEGORY;
import static nl.maastro.medstruct.medstructgui.constants.MedStructConstants.LAYER_CONTEXT_TARGET;

public class JsonNlpUtil {


    public static JsonNode getJsonNode(String jsonNlp, ObjectMapper objectMapper) throws IOException {
        return objectMapper.readTree(jsonNlp);
    }


    public static JsonNode getDocument(JsonNode jsonNlpNode) {
        return jsonNlpNode.get("documents").get(0);
    }


    public static String refreshDocument(JsonNode jsonNlpNode, JsonNode document, ObjectMapper objectMapper) throws JsonProcessingException {
        ((ArrayNode) jsonNlpNode.withArray("documents")).set(0, document);
        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNlpNode);
    }

    public static JsonNode addModuleResult(JsonNode document, JsonNode jsonNode, String key) {
        ((ObjectNode) document).put(key, jsonNode);
        return document;

    }

    public static JsonNode createJsonNode(Object object, ObjectMapper objectMapper){
        return objectMapper.valueToTree(object);
    }


    public static boolean isInSameSentence(JsonNode document, int token1, int token2) {
        return JsonNlpUtil.getSentenceId(document, token1) == (JsonNlpUtil.getSentenceId(document, token2));
    }

    public static Integer getSentenceId(JsonNode document, int token_id) {
        return document.get("tokenList").get(token_id - 1).get("sentence_id").asInt();
    }

    public static Integer getSentenceTokenFrom(JsonNode document, String sentenceId){
        return document.get("sentences").get(sentenceId).get("tokenFrom").asInt();
    }

    public static String getContextTargetUri(JsonNode contextElement) {
        return contextElement.get(LAYER_CONTEXT_TARGET).get(LAYER_CONTEXT_CATEGORY).get(0).textValue();
    }

    public static List<String> getTokenUris(JsonNode token) {
           List<String> uris = new ArrayList<>();
            if(token.get("misc").has("uri")) {
                Iterator<JsonNode> itr = token.get("misc").get("uri").iterator();
                while (itr.hasNext()) uris.add(itr.next().asText());
            }
            return uris;
    }

    public static List<Integer> getSentenceIdsForTokens(JsonNode document, List<Integer> tokens){
        return tokens.stream().map(token -> getSentenceId(document, token)).collect(Collectors.toList());
    }


    public static Iterator<JsonNode> getTokens(JsonNode document){
        return document.get("tokenList").iterator();
    }


    public static List<JsonNode> getTokensForSentenceId(JsonNode document, int sentenceId) {
        List<JsonNode> tokens = new ArrayList<>();
        document.get("tokenList").iterator().forEachRemaining(token -> {
            if(token.get("sentence_id").asInt() == sentenceId) tokens.add(token);
        });
        return tokens;
    }

    public static JsonNode getSentence(JsonNode document, int sentenceId) {
        return document.get("sentences").get(Integer.toString(sentenceId));
    }

    public static List<Integer> getUriSentenceIds(JsonNode document, String uri){
        List<Integer> sentenceIds = new ArrayList<>();
        document.get("tokenList").iterator().forEachRemaining(token -> {
            if(token.has("misc")
                    && token.get("misc").has("uri")) {
                Iterator<JsonNode> itr = token.get("misc").get("uri").iterator();
                while (itr.hasNext()) {
                    if(itr.next().textValue().equals(uri)){
                        sentenceIds.add(token.get("sentence_id").asInt());
                    }
                }
            }
        });
        return sentenceIds.stream().distinct().collect(Collectors.toList());
    }

    public static String getText(JsonNode token) {
        return token.get("text").textValue();
    }

    public static Iterator<JsonNode> getMeasurements(JsonNode document) {
        return document.get("measurements").iterator();
    }

    public static String getMeasurementExpression(JsonNode measurement) {
        return measurement.get("expression").textValue();
    }

    public static String getMeasurementCode(JsonNode measurement) {
        if(measurement.has("code"))
            return measurement.get("code").textValue();
        else
            return null;
    }

    public static Iterator<JsonNode> getContext(JsonNode document) {
        return document.get("context").elements();
    }

    public static Iterator<JsonNode> getContextModifier(JsonNode contextNode) {
        return contextNode.get("modifiers").elements();
    }

    public static Iterator<JsonNode> getPresentConcepts(JsonNode document){
        return document.get("tnm-classification").get("presentConcepts").iterator();
    }

    public static Iterator<JsonNode> getInvolvedConcepts(JsonNode document){
        return document.get("tnm-classification").get("involvedConcepts").iterator();
    }
    
    public static String textFromTokens(JsonNode document, Iterator<JsonNode> tokenIds){
        StringBuilder stringBuilder = new StringBuilder();

        while (tokenIds.hasNext()){
            JsonNode token = getToken(document, tokenIds.next().asInt());
            stringBuilder.append(token.get("text").asText());
            if(tokenIds.hasNext() && token.get("misc").get("SpaceAfter").asBoolean()){
                stringBuilder.append(" ");
            }
        }
        return stringBuilder.toString();
    }

    public static JsonNode getToken(JsonNode document, Integer tokenId){
        return document.get("tokenList").get(tokenId - 1);
    }
}

