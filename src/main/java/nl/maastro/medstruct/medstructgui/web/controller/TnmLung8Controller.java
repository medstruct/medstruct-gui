package nl.maastro.medstruct.medstructgui.web.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import nl.maastro.medstruct.medstructgui.entity.TnmLung8FeedbackEntity;
import nl.maastro.medstruct.medstructgui.respository.TnmLung8FeedbackRepository;
import nl.maastro.medstruct.medstructgui.service.DictionaryService;
import nl.maastro.medstruct.medstructgui.service.MedstructService;
import nl.maastro.medstruct.medstructgui.util.JsonNlpUtil;
import nl.maastro.medstruct.medstructgui.web.controller.dto.MedstructRequestDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/tnm/lung8")
public class TnmLung8Controller {

    private static final Logger logger = LoggerFactory.getLogger(TnmLung8Controller.class);

    private ObjectMapper objectMapper;

    private MedstructService medstructService;

    private DictionaryService dictionaryService;

    private final TnmLung8FeedbackRepository tnmLung8FeedbackRepository;


    public TnmLung8Controller(ObjectMapper objectMapper,
                              MedstructService medstructService,
                              DictionaryService dictionaryService,
                              TnmLung8FeedbackRepository tnmLung8FeedbackRepository) {
        this.objectMapper = objectMapper;
        this.medstructService = medstructService;
        this.dictionaryService = dictionaryService;
        this.tnmLung8FeedbackRepository = tnmLung8FeedbackRepository;
    }

    @GetMapping("/jsonnlp")
    public String postTextGet(
            @RequestParam(required = false, defaultValue = "text") String text,
            @RequestParam(required = false, defaultValue = "en") String lang ,
            @RequestParam(required = false, defaultValue = "spellcheck") String method ) {
       return this.handleJsonNlpRequest(text, lang, method);
    }

    @PostMapping("/jsonnlp")
    public String postText(
            @RequestBody  MedstructRequestDto medstructRequestDto) {
        return handleJsonNlpRequest(medstructRequestDto.getText(), medstructRequestDto.getLang(), medstructRequestDto.getMethod());
    }

    private String handleJsonNlpRequest(String text, String lang, String method){
        JsonNode jsonNode = objectMapper.createObjectNode();

        try {
            String response = medstructService.processPipeline("" + text.hashCode(), text, lang, method);
            JsonNode tree = objectMapper.readTree(response);
            JsonNode document = JsonNlpUtil.getDocument(tree);

            ((ObjectNode) jsonNode).set("words", dictionaryService.getWords(document));
            ((ObjectNode) jsonNode).set("json-nlp", tree);
        }
        catch(Exception e){
            logger.error("Error processing request", e);
            ((ObjectNode) jsonNode).put("error", e.getMessage());
        }

        return jsonNode.toString();
    }

    @PostMapping("/feedback")
    public TnmLung8FeedbackEntity postFeedback(
            @RequestBody TnmLung8FeedbackEntity feedbackEntity) {
        return tnmLung8FeedbackRepository.save(feedbackEntity);
    }

    @GetMapping("/feedback/count")
    public long countFeedBack() {
        return tnmLung8FeedbackRepository.count();
    }

}
