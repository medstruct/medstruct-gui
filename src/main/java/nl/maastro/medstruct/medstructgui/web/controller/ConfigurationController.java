package nl.maastro.medstruct.medstructgui.web.controller;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import nl.maastro.medstruct.medstructgui.configuration.properties.MedstructGuiProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/config")
@EnableConfigurationProperties(MedstructGuiProperties.class)
public class ConfigurationController {

    private MedstructGuiProperties medstructGuiProperties;

    public ConfigurationController(MedstructGuiProperties medstructGuiProperties) {
        this.medstructGuiProperties = medstructGuiProperties;
    }

    @GetMapping("/default-language")
    private String getDefaultLanguage(){
        JsonNode node = new ObjectMapper().createObjectNode();
        ((ObjectNode) node).put("language", medstructGuiProperties.getDefaultLanguage());
        return node.toPrettyString();
    }
}
