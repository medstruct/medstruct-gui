package nl.maastro.medstruct.medstructgui.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Entity;
import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class TnmLung8FeedbackEntity {
    @Id @GeneratedValue
    private Long id;

    private Date date = new Date();

    @Column(columnDefinition = "TEXT")
    private String value;

}