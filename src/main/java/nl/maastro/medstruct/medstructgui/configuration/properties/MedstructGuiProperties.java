package nl.maastro.medstruct.medstructgui.configuration.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix="medstruct-gui")
public class MedstructGuiProperties {

    private String medstructTnmLung8ApiUrl = "http://localhost:8081/api/tnm/lung-8";

    private String defaultLanguage = "nl";

    public String getMedstructTnm8LungApiUrl() {
        return medstructTnmLung8ApiUrl;
    }

    public void setMedstructTnmLung8ApiUrl(String medstructTnmLung8ApiUrl) {
        this.medstructTnmLung8ApiUrl = medstructTnmLung8ApiUrl;
    }

    public String getMedstructTnmLung8ApiUrl() {
        return medstructTnmLung8ApiUrl;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }
}
