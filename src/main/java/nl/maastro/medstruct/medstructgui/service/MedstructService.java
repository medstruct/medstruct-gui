package nl.maastro.medstruct.medstructgui.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import nl.maastro.medstruct.medstructgui.configuration.properties.MedstructGuiProperties;
import nl.maastro.medstruct.medstructgui.web.controller.dto.MedstructRequestDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@EnableConfigurationProperties({MedstructGuiProperties.class})
public class MedstructService {

    private static final Logger logger = LoggerFactory.getLogger(MedstructGuiProperties.class);

    private RestTemplate restTemplate;

    private MedstructGuiProperties medstructGuiProperties;

    private ObjectMapper objectMapper;

    public MedstructService(RestTemplate restTemplate, MedstructGuiProperties medstructGuiProperties) {
        this.restTemplate = restTemplate;
        this.medstructGuiProperties = medstructGuiProperties;
    }

    public String processPipeline(String identifier, String text, String lang, String method){
        MedstructRequestDto requestDto = new MedstructRequestDto();
        requestDto.setIdentifier(identifier);
        requestDto.setText(text);
        requestDto.setLang(lang);
        requestDto.setMethod(method);
        ResponseEntity<String> response = restTemplate.postForEntity(medstructGuiProperties.getMedstructTnm8LungApiUrl(), requestDto, String.class);
        return response.getBody();
    }



}
