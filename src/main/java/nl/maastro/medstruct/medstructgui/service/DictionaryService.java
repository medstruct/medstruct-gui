package nl.maastro.medstruct.medstructgui.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import nl.maastro.medstruct.medstructgui.util.JsonNlpUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DictionaryService {

    private static final Logger logger = LoggerFactory.getLogger(DictionaryService.class);

    private ObjectMapper objectMapper;

    public DictionaryService(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public JsonNode getWords(JsonNode document){
        ObjectNode words = objectMapper.createObjectNode();
        words = getMeasurements(document, words);
        words = getPresentConcepts(document, words);
        words = getInvolvedConcepts(document, words);
        words = getContext(document, words);
        return words;
    }

    public ObjectNode getPresentConcepts(JsonNode document, ObjectNode words){
        Iterator<JsonNode> presentConceptsItr = JsonNlpUtil.getPresentConcepts(document);
        while(presentConceptsItr.hasNext()){
            JsonNode concept = presentConceptsItr.next();
            String text = JsonNlpUtil.textFromTokens(document, concept.get("tokens").elements());
            updateWords(words, text, "PRESENT", concept.get("uri").textValue());
        }
        return words;
    }


    public ObjectNode getInvolvedConcepts(JsonNode document, ObjectNode words){
        Iterator<JsonNode> presentConceptsItr = JsonNlpUtil.getInvolvedConcepts(document);
        while(presentConceptsItr.hasNext()){
            JsonNode presentConcept = presentConceptsItr.next();
            String text = JsonNlpUtil.textFromTokens(document, presentConcept.get("tokens").elements());
            updateWords(words, text, "INVOLVED", presentConcept.get("uri").textValue());
        }
        return words;
    }

    public ObjectNode getMeasurements(JsonNode document, ObjectNode words){
        Iterator<JsonNode> measurements = JsonNlpUtil.getMeasurements(document);
        while (measurements.hasNext()) {
            JsonNode measurement = measurements.next();
            String text = JsonNlpUtil.getMeasurementExpression(measurement);
            String uri = JsonNlpUtil.getMeasurementCode(measurement);
            updateWords(words, text, "MEASUREMENT", uri);
        }
        return words;
    }

    public ObjectNode getContext(JsonNode document, ObjectNode words) {
        Iterator<JsonNode> context = JsonNlpUtil.getContext(document);
        while (context.hasNext()) {
            JsonNode contextNode = context.next();

            StringBuilder uriBuilder = new StringBuilder();
            try {
                if(contextNode.get("target").has("category"))
                    uriBuilder.append(objectMapper.writeValueAsString(contextNode.get("target").get("category")));
            } catch (JsonProcessingException e) {
                logger.error("Error writing contextUri", e);
            }

            StringBuilder phraseBuilder = new StringBuilder();
            phraseBuilder.append(JsonNlpUtil.textFromTokens(document, contextNode.get("target").get("tokens").iterator()) + " ");

            List<String> modifiers = new ArrayList<>();
            contextNode.get("modifiers").iterator().forEachRemaining(
                    mod -> {
                        String modString = JsonNlpUtil.textFromTokens(document, mod.get("tokens").iterator());
                        if(!modifiers.contains(modString)) modifiers.add(modString);
                    });
            String modifiersString = modifiers.stream()
                    .map(n -> String.valueOf(n))
                    .collect(Collectors.joining(", ", "", ""));
            phraseBuilder.append(" [ modifiers: " + modifiersString + " ]");


            updateWords(words, modifiersString, "CONTEXT", phraseBuilder.toString());
        }
        return words;
    }


    private ObjectNode updateWords(ObjectNode words, String text, String label, String uri){
        String[] splitted = text.trim().split(" ");
        for(String s : splitted){
            ArrayNode array;
            if(words.get(s) != null)
                array = (ArrayNode) words.get(s);
            else
                array = objectMapper.createArrayNode();
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(label + " | ");
            stringBuilder.append(text + " | ");
            if(uri != null)
                stringBuilder.append(uri);
            else
                stringBuilder.append("no-uri");
            array.add(stringBuilder.toString());
            words.set(s, array);
        }
        return words;
    }
}
