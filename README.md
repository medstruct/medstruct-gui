# MEDSTRUCT-GUI

Graphical user interface for MedStruct.

## RUN

Run MEDSTRUCT using docker-compose ([instructions](https://github.com/putssander/medstruct-config)).

![Alt text](/doc/MEDSTRUCT_GUI_2020-01-15.png?raw=true "MEDSTRUCT GUI")

## BUILD

   frontend (from ./client)
   
    ng build --prod --outputPath=./../src/main/resources/static/

   backend (from ./)

    ./mvnw clean package fabric8:build -DskipTests -Dfabric8.profile=putssander
    
## Config

medstruct-gui.default-language: en