import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {environment} from '../../environments/environment';

import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {TextdataService} from '../shared/data/textdata.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Tnmlung8feedbackService} from '../shared/tnmlung8feedback/tnmlung8feedback.service';
import {EventObj} from '@tinymce/tinymce-angular/editor/Events';
import {Tnmlung8classifyService} from '../shared/tnmlung8classify/tnmlung8classify.service';
import {Tnmlung8classify} from '../model/tnmlung8classify.model';
import {DomSanitizer, SafeHtml} from '@angular/platform-browser';

@Component({
  selector: 'app-tnmlung8',
  templateUrl: './tnmlung8.component.html',
  styleUrls: ['./tnmlung8.component.sass'],
  encapsulation: ViewEncapsulation.None,

})
export class Tnmlung8Component implements OnInit {

  medstructApiUrl = environment.medstructApiUrl;

  validateForm: FormGroup;
  dataFile: string;
  tinyMCE: any;

  classificationResult: {};

  annotationActive: boolean;
  classificationActive: boolean;
  private interval: any;
  private timer: any;

  submitOk: boolean;
  submitId: number;
  submitError: boolean;
  submitErrorMessage: string;

  jsonNlpErrorMessage: string;

  autoClassification: boolean;

  jsonNlpHtml: SafeHtml;
  showAnnotated: boolean;
  language: string;

  constructor(private fb: FormBuilder,
              private textDataService: TextdataService,
              private http: HttpClient,
              private feedbackService: Tnmlung8feedbackService,
              private classifyService: Tnmlung8classifyService,
              private sanitized: DomSanitizer) { }

  ngOnInit() {
    this.initForm();
    this.classificationActive = false;
    this.annotationActive = false;
    this.autoClassification = false;
    this.showAnnotated = false;
    this.tinyMCE = window["tinyMCE"];
    this.language = "nl";
    this.setDefaultLanguage();
  }

  initForm(){
    this.validateForm = this.fb.group({
      tumorSize: null,
      tumorSide: "UNKNOWN",

      presentConcepts: [],
      involvedConcepts: [],

      satelliteNodules: null,
      tumorsInDiffentLobesIpsilateral: null,

      lymphNodes: [],

      tnmT: new FormControl(null, Validators.requiredTrue),
      tnmN: new FormControl(null, Validators.requiredTrue),
      tnmM: new FormControl(null, Validators.requiredTrue),

      tnmedit: false,
      reportedit: false,

      externalId: null,
      comment: null
    });
  }

  submitForm(): void {
    const result = {
      "form": this.validateForm.value,
      "algorithm": this.classificationResult
    };
    this.feedbackService.save(result)
      .subscribe(
        result => {
          this.submitOk = true;
          this.submitId = result.id;
        },
        error => {
          this.submitError = true;
          this.submitErrorMessage = error.message;
        }
      )
  }

  getEditorConfiguration(){
    let self = this;
    return {
      'base_url': '/tinymce',
      'suffix': '.min',
      'plugins': 'paste',
      'toolbar': 'undo redo',
      'content_css': '/assets/tinymce-tnm.css',
      'menubar': false,
      'height': '450px'
    };
  }

  annotateButton(){
    this.annotate(true);
  }

  annotate(show: boolean){
    let text = this.tinyMCE.activeEditor.getContent({format : 'text'});
    if(text.trim().length === 0) return;

    if(show)
      this.tinyMCE.activeEditor.setProgressState(true);
    this.classificationActive = true;
    this.annotationActive = true;

    const config = {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    };
    const data = {
      text: text,
      lang: this.language,
      method: 'spellcheck'
    };

    this.http.post(this.getSpellCheckerUrl(), data, config).subscribe(resp => {
        this.tinyMCE.activeEditor.setProgressState(false);
        if('error' in resp){
          this.handleJsonNlpError(resp);
        }
        else {
          this.parseJsonNlp(resp);
          setTimeout(() => {
            this.classificationActive = false;
            this.annotationActive = false;
          }, 10);

          if (show)
            this.showAnnotated = true;
        }
      },
      error => {
          this.handleJsonNlpError(null);
      }
    );
  }

  handleJsonNlpError(resp){
    this.jsonNlpErrorMessage = " ";
    this.tinyMCE.activeEditor.setProgressState(false);
    this.classificationActive = false;
    this.annotationActive = false;
    if(resp && 'error' in resp){
      this.jsonNlpErrorMessage = String(resp['error']);
    }
    setTimeout(()=>{this.jsonNlpErrorMessage=null}, 5000)
  }

  loadData(value: string) : void {
    if(value.includes('lang-en'))
      this.language = "en";
    if(value.includes('lang-nl'))
      this.language = "nl";

    this.textDataService.getText(value).subscribe((data)=>{
      this.setTinyMceContent(data);
      if(this.autoClassification)
        this.preformAutoCheck();
    });
  }

  setTinyMceContent(content: string){
    this.showAnnotated = false;
    this.tinyMCE.activeEditor.setContent(content.replace(/\r?\n/g, '<br/>'), {format : 'text'});
  }

  private getSpellCheckerUrl() : string {
    return this.medstructApiUrl + '/api/tnm/lung8/jsonnlp'
  }

  private getDefaultLanguageUrl() : string {
    return this.medstructApiUrl + '/api/config/default-language'
  }

  parseJsonNlp(data: any){
    if(!data['json-nlp']|| !data['json-nlp']['documents'][0]['tnm-classification'])
      return;

    const document = data['json-nlp']['documents'][0];
    const tnmClass = document['tnm-classification'];

    this.classificationResult = tnmClass;
    this.jsonNlpHtml =  this.sanitized.bypassSecurityTrustHtml(this.tokenSpan(document));
    setTimeout(() => this.addTnmClasses(document), 1);
    this.setForm(tnmClass);
  }

  setForm(tnmClass){
    let presentConcepts = [];
    let involvedConcepts = [];
    let lymphNodes = [];

    tnmClass.presentConcepts.forEach(item => {if(!presentConcepts.includes(item.uri)) presentConcepts.push(item.uri)});
    tnmClass.involvedConcepts.forEach(item => {if(!involvedConcepts.includes(item.uri)) involvedConcepts.push(item.uri)});
    tnmClass.lymphNodes.forEach(item => this.addLymphNode(item, lymphNodes));

    this.validateForm.patchValue({presentConcepts: presentConcepts});
    this.validateForm.patchValue({involvedConcepts: involvedConcepts});
    this.validateForm.patchValue({lymphNodes: lymphNodes});
    this.validateForm.patchValue({tumorSize: tnmClass.tumorSize});
    this.validateForm.patchValue({tumorSide: tnmClass.tumorSide});
    this.validateForm.patchValue({satelliteNodules: tnmClass.satelliteNodules});
    this.validateForm.patchValue({tumorsInDiffentLobesIpsilateral: tnmClass.tumorsInDiffentLobesIpsilateral});
    this.validateForm.patchValue({tnmT: tnmClass.tnmT});
    this.validateForm.patchValue({tnmN: tnmClass.tnmN});
    this.validateForm.patchValue({tnmM: tnmClass.tnmM});
  }

  resetForm(){
    this.initForm();
    this.submitOk = false;
    this.submitError = false;
    this.submitErrorMessage ="";
    this.submitId = null;
  }

  closeSubmitResponse(){
    this.submitOk = false;
    this.submitError = false;
    this.submitErrorMessage = "";
    this.submitId = null;
  }

  addLymphNode(item, lymphNodes){
    switch(item.bodySide) {
      case "LEFT":
        this.addLymphNodeLeft(item, lymphNodes);
        break;
      case "RIGHT":
        this.addLymphNodeRight(item, lymphNodes);
        break;
      default:
        this.addLymphNodeUnknown(item, lymphNodes);
    }
  }

  addLymphNodeUnknown(item, list){
    if(!list.includes(item.uri)) list.push(item.uri);
  }

  addLymphNodeLeft(item, list){
    const uri = item.uri + "-left";
    if(!list.includes(uri)) list.push(uri);
  }

  addLymphNodeRight(item, list){
    const uri = item.uri + "-right";
    if(!list.includes(uri)) list.push(uri);  }

  /**
   * I know this is not very clean
   * @param $event
   */
  keyUpEvent($event: EventObj<KeyboardEvent>) {
    if(!this.autoClassification)
      return;

    if (this.timer) {
      clearTimeout(this.timer);
      this.timer = 0;
    }
    this.timer = setTimeout(() => this.preformAutoCheck(), 1500);
  }

  preformAutoCheck() {
    this.classificationActive = false;
    this.interval = setInterval(() => {
      if (this.classificationActive)
        clearInterval(this.interval);
      else
        setTimeout(() => this.annotate(false), 1);
    }, 10);
  }

  classify(){
    if(this.classificationActive || this.annotationActive)
      return;

    this.classificationActive = true;
    let tnmlung8classify = new Tnmlung8classify();
    tnmlung8classify.tumorSide = this.validateForm.value.tumorSide;
    tnmlung8classify.tumorSize = this.validateForm.value.tumorSize;

    tnmlung8classify.presentConcepts = [];
    tnmlung8classify.involvedConcepts = [];
    if(this.validateForm.value.presentConcepts)
      tnmlung8classify.presentConcepts = this.validateForm.value.presentConcepts;
    if(this.validateForm.value.involvedConcepts)
      tnmlung8classify.involvedConcepts = this.validateForm.value.involvedConcepts;

    tnmlung8classify.satelliteNodules = this.validateForm.value.satelliteNodules;
    tnmlung8classify.tumorsInDiffentLobesIpsilateral = this.validateForm.value.tumorsInDiffentLobesIpsilateral;

    tnmlung8classify.lymphNodes = [];
    if(this.validateForm.value.lymphNodes)
      tnmlung8classify.lymphNodes = tnmlung8classify.lymphNodes.concat(this.validateForm.value.lymphNodes);

    this.classifyService.classify(tnmlung8classify).subscribe(
      result => {
        this.validateForm.patchValue({tnmT: result.tnmT});
        this.validateForm.patchValue({tnmN: result.tnmN});
        this.validateForm.patchValue({tnmM: result.tnmM});
        this.classificationActive = false;
      },
      error => {
        this.classificationActive = false;
      });
  }

  tokenSpan(jsonNlpDocument){
    let rawHtml = "";
    jsonNlpDocument["tokenList"].forEach(token => {
      rawHtml+="<span id=\"token-"+token['id'] + "\">" + token['text'] + "</span>";
      if(token['misc']['SpaceAfter'])
        rawHtml+=" ";
    });
    return rawHtml;
  }

  addTnmClasses(jsonNlpDocument){
    const context = jsonNlpDocument['context'].forEach(contextItem => {
      const target = contextItem['target']['tokens'].forEach(tokenId => this.addClassToTokenId(tokenId, "context-target"));
      contextItem['modifiers'].forEach(modifier => {
        modifier['tokens'].forEach(tokenId => this.addClassToTokenId(tokenId, 'context-modifier'));
      });
    });

    const tnmClass = jsonNlpDocument['tnm-classification'];
    tnmClass.presentConcepts.forEach(concept => concept['tokens'].forEach(tokenId => this.addClassToTokenId(tokenId, "tnm-present")));
    tnmClass.involvedConcepts.forEach(concept => concept['tokens'].forEach(tokenId => this.addClassToTokenId(tokenId, "tnm-involved")));
  }

  addClassToTokenId(tokenId: number, cssClass: string){
    document.getElementById("token-"+tokenId).classList.add(cssClass);
  }

  setDefaultLanguage() {
    this.http.get(this.getDefaultLanguageUrl()).subscribe(resp => {
      this.setLanguage(resp);
    });
  }

  setLanguage(resp){
    this.language = resp['language'];
  }

}
