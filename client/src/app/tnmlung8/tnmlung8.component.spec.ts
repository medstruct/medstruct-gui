import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Tnmlung8Component } from './tnmlung8.component';

describe('Tnmlung8Component', () => {
  let component: Tnmlung8Component;
  let fixture: ComponentFixture<Tnmlung8Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Tnmlung8Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Tnmlung8Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
