export class Tnmlung8classify {
  constructor(
    public tumorSize?: number,
    public tumorSide?: string,
    public presentConcepts?: string[],
    public involvedConcepts?: string[],
    public lymphNodes?: string[],
    public satelliteNodules?: boolean,
    public tumorsInDiffentLobesIpsilateral?: boolean,
  ){}
}
