export class Tnmlung8feedback {

  constructor(
    public id?: number,
    public tumorSize?: number,
    public tumorSide?: string,
    public tumors?: string[],
    public presentConcepts?: string[],
    public involvedConcepts?: string[],
    public lymphNodes?: string[],
    public tumorsDiffLobesIps?: boolean,
    public tnm?: string,
    public t?: string,
    public n?: string,
    public m?: string,
    public tnmedit?: boolean,
    public reportedit?: boolean,
    public comment?: string,
    public externalId?: string,
  ){}
}
