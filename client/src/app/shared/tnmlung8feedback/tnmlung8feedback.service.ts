import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class Tnmlung8feedbackService {
  public API = environment.medstructApiUrl;
  public TNM_LUNG8_API = this.API + '/api/tnm/lung8/feedback';

  constructor(private http: HttpClient) {
  }

  save(tnmlung8feedback: any): Observable<any> {
    console.log("submit");
    let result: Observable<any>;
    const body = {
      value: JSON.stringify(tnmlung8feedback)
    }
    if (tnmlung8feedback.href) {
      result = this.http.put(tnmlung8feedback.href, body);
    } else {
      result = this.http.post(this.TNM_LUNG8_API, body);
    }
    return result;
  }
}
