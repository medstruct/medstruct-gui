import { TestBed } from '@angular/core/testing';

import { TextdataService } from './textdata.service';

describe('TextdataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TextdataService = TestBed.get(TextdataService);
    expect(service).toBeTruthy();
  });
});
