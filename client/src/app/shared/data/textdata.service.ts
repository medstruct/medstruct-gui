import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TextdataService {

  medstructApiUrl = environment.medstructApiUrl;

  constructor(private http: HttpClient) {
  }

  getText(text: string): Observable<string> {
    const url = this.medstructApiUrl + '/assets/data/' + text;
    return this.http.get(url, {responseType: 'text'});
  }}
