import { Injectable } from '@angular/core';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Tnmlung8classify} from '../../model/tnmlung8classify.model';

@Injectable({
  providedIn: 'root'
})
export class Tnmlung8classifyService {

  public API = environment.medstructApiUrl;
  public TNM_LUNG8_API = this.API + '/medstruct-tnm-classifier/api/classify/tnm/lung8';

  constructor(private http: HttpClient) {
  }

  classify(tnmlung8classify: Tnmlung8classify): Observable<any> {
    console.log("classify");
    let result: Observable<any>;
    result = this.http.post(this.TNM_LUNG8_API, tnmlung8classify);
    return result;
  }
}
