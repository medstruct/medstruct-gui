import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {Tnmlung8Component} from './tnmlung8/tnmlung8.component';
import {EditorModule} from '@tinymce/tinymce-angular';
import {
  NgZorroAntdModule,
  NZ_I18N,
  en_US,
  NzSelectModule,
  NzGridModule,
  NzLayoutModule,
  NzInputModule,
  NzMentionModule
} from 'ng-zorro-antd';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';

registerLocaleData(en);


@NgModule({
  declarations: [
    AppComponent,
    Tnmlung8Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    EditorModule,
    NgZorroAntdModule,
    NzLayoutModule,
    NzGridModule,
    FormsModule,
    NzSelectModule,
    NzInputModule,
    NzMentionModule,
    ReactiveFormsModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
