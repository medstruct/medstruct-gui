Tumor visible in the right upper lobe (5 cm).
Invasion of the main bronchus.
Invasion of the visceral pleura and atelectasis and postobstructive pneumonitis.
The tumor involves the chest wall and the pericardium.
There is also involvement of the phrenic nerve.
Satellite nodules are present in the right lower lobe.
There is invasion of the mediastinum and diaphragm.
The heart and great vessels are involved as well.
Involvement of the laryngeal nerve and carina.
The tumor invades the trachea and invades the oesophagus.
Invasion of the spine is present.
Different tumor of 4 cm in the middle lobe.
